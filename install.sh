#!/usr/bin/env bash

echo Welcome to Zyrophr\'s dotfiles linking script.
echo
echo When entering \"y\" to install the files for a specific software \(i.e.: vim\) it
echo will symink the file from the current folder to the appropriate place in your
echo file system. \(i.e.: ./vim/.vimrc ==\> ~/.vimrc\)
echo
echo Entering \"y\" will NOT install the corresponding packages. See install.sh for
echo this.

HERE="$(pwd)/$(dirname "$0")"
BACKUP_DIR="$HERE/dotfiles_backups/"

function ask_folder() {
	read -p "Do you want to install $1 ? " -n 1 -r
	echo
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		return 0
	fi
	return 1
}

function sym() {
	DEST_FILE="$(basename "$1")"
	DEST="$2$DEST_FILE"
	HERESRC="$HERE/$1"
	echo $HERESRC -\> $DEST
	if [[ -f $DEST || -L $DEST || -d $DEST ]]; then
		BACKUP_DESTDIR=$BACKUP_DIR"$(dirname "$1")"
		echo Destination already exists! Backuping to "$BACKUP_DESTDIR/$DEST_FILE"
		mkdir -p "$BACKUP_DESTDIR"
		mv "$DEST" "$BACKUP_DESTDIR/"
	fi
	ln -s $HERESRC $DEST
	echo Symlink: done.
}

mkdir -p $BACKUP_DIR
mkdir -p "$HOME/.config"

if ask_folder ".config"; then
	SRC=".config"
	sym "$SRC/comton" "$HOME/.config/"
	sym "$SRC/i3" "$HOME/.config/"
	sym "$SRC/polybar" "$HOME/.config/"
	sym "$SRC/neofetch" "$HOME/.config/"
	sym "$SRC/ranger" "$HOME/.config/"
fi

if ask_folder shellshit; then
	sym ".aliases" "$HOME/"
	sym ".zprezto" "$HOME/"
	sym ".zpreztorc" "$HOME/"
	sym ".zprofile" "$HOME/"
	sym ".zshrc" "$HOME/"
fi

if ask_folder vim; then
	sym ".vimrc" "$HOME/"
	sym ".vim_runtime" "$HOME/"
fi

if ask_folder Xresources; then
	sym ".Xresources" "$HOME/"
fi

if ask_folder Scripts; then
	sym "Scripts" "$HOME/"
fi

if ask_folder music; then
	sym ".mpd" "$HOME/"
	sym ".ncmpcpp" "$HOME/"
fi

echo
echo Done importing Dotfiles!
echo
