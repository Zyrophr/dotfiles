#!/bin/sh

c0='#00000000' # clear
c1='#101012ff' # black
c2='#ffffffff' # white
c3='#ff0000ff' # red
c4='#0000ffff' # blue
c5='#e577f2ff' #pink
c6='#3b3774ff' #purple

over_path='/tmp/bg.png'
height=$(xdpyinfo | awk '/dimensions/{print $2}' | cut -dx -f 2)

over=/home/zyrophr/Images/zyrophr/transp.png

convert "$over" -resize "x$height>" "$over_path"

i3lock \
--insidevercolor=$c0	\
--ringvercolor=$c6	\
--insidewrongcolor=$c0	\
--ringwrongcolor=$c3	\
\
--insidecolor=$c0	\
--ringcolor=$c0		\
--separatorcolor=$c0	\
--linecolor=$c0		\
\
--timecolor=$c5		\
--datecolor=$c5		\
--keyhlcolor=$c5	\
--bshlcolor=$c5		\
\
--blur 5                \
--clock                 \
--indicator             \
--radius 180		\
--ring-width 10		\
--timestr="%H:%M"	\
--datestr="%A, %B %d."	\
\
--veriftext=""		\
--wrongtext=""		\
--timesize=80		\
--datesize=20		\
--image=$over_path      \

