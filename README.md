# Rice
This repo contains all my dotfiles, used to build my new 2018 rice! I didn't do everything, I just used some part of other dotfiles from /r/unixporn, and tweaked them, so feel free to steal <3

## My setup :
![rice](https://i.imgur.com/V90aecf.png)
(this is just a sample of what type of rice you can make with those files. If you want to see more examples, just slide to the bottom of this readme !)

- Wm : i3-gaps
- Bars: polybar
- Editor: Visual Studio Code & Vim
- Music player: ncmpcpp + mpd
- Visualizer: cava
- Matrix thing : cmatrix
- Task manager : htop
- menu : rofi
- file explorer : Nautilus
- system info : neofetch

Pay attention, if you want to use my dotfiles, you still will have to install some packages.


## My packages :
Here are the packages I installed on my computer.
This repo might contains some of their config file (but not in most case)
- zsh (main shell w/ Prezto)
- discord
- Firefox (main browser)
- Visual Studio Code (main editor)
- Vim (Terminal Editor)
- htop (main task manager)
- neofetch
- rofi (main menu)
- ranger (main file explorer)
- Nautilus (GUI file explorer)
- ncmpcpp + mpd (main music player)
- i3-gaps + i3lock-color
- feh + imagemagick
- rxvt-unicode (main terminal)
- polybar (well it's... the bar...)
- light (brightness control, which is, actually, working strangely...)
- Hack & FontAwesome (main fonts & glyphs)
- compton (only used for transparency and blur)
- w3m
- weechat


## Informations

My OS is, as you can see on the screenshots, *Fedora 28*. 
**It may not work on your computer, because of how are built the different OS!**

This is a WIP, I need to make a tutorial on how my setup works, and maybe how to install all this stuff. But you should be able to figure it out by yourself <3

## Other examples :

![example](https://i.imgur.com/ojNURW6.png)

![example](https://i.imgur.com/hatEe7L.png)

![example](https://i.imgur.com/JEti8fn.png)

![example](https://i.imgur.com/OAvAC18.png)

![example](https://i.imgur.com/cECRlfj.png)